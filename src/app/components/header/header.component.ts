import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'gs-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input('loggedUser') user: string;
  @Output() searchUser = new EventEmitter();
  @Output() goHome = new EventEmitter();

  constructor(private router: Router) { }

  userName: String = '';
  ngOnInit() {
  }

  logout(){
    sessionStorage.removeItem('token');
    this.router.navigate(['signin'])

  }

  findUser(searchForm){
    this.searchUser.emit(this.userName);
  }

  home(){
    this.goHome.emit(true);
  }

}
