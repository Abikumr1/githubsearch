import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup} from '@angular/forms';
import { GitHubService } from '../../service/github.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AccessToken } from '../../module/shared/model';
import { Subscription } from 'rxjs';

@Component({
    selector: 'gs-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
    loginForm: FormGroup;
    isAuthorised: boolean;
    code: string = '';    
    private subcription: Subscription = new Subscription();

    constructor( private gitService: GitHubService,
                 private activeRoute: ActivatedRoute,
                 private router: Router) { }

    ngOnInit() {

        this.activeRoute.paramMap.subscribe(value =>{
            console.log(value);
        })
        this.isAuthorised = this.activeRoute.snapshot.queryParamMap.has('code');
        this.code = this.activeRoute.snapshot.queryParamMap.get('code');
        //console.log(`code: ${this.code}: ${this.isAuthorised}`)

        if(this.isAuthorised){
            this.getGitToken(this.code)
        }

        
     }

     getGitToken(code){
        this.subcription = this.gitService.getGitAuthToken(this.code).subscribe((token: AccessToken)=> {
            if(token.access_token){
              sessionStorage.setItem('token', token.access_token);
              this.router.navigate(['/dashboard'])
            }
        }); 
     }

    onSubmit() { }

    // loginWithGit() {
    //     //this.router.
    //     this.activeRoute.paramMap.subscribe(value =>{
    //         console.log(value);
    //     })
    //     this.isAuthorised = this.activeRoute.snapshot.queryParamMap.has('code');
    //     this.code = this.activeRoute.snapshot.queryParamMap.get('code');
    //     console.log(`code: ${this.code}`)

    //     this.gitService.getGitAuthToken(this.code).subscribe((token: AccessToken)=> {
    //         if(token.access_token)
    //           sessionStorage.setItem('token', token.access_token);
    //     });
    // }

    signconfigData = [
        { url: "https://github.com/login/oauth/authorize?client_id=bd03d44d0fd4e0cd19f3&scope=repo,delete_repo,admin:org",
          icon: "fa fa-github",
          btnColor: "btn-success",
          msg: "Sign in with GitHub"
        },
        { url: "#",
          icon: "fa fa-facebook",
          btnColor: "btn-primary",
          msg: "Sign in with Facebook"
        },
        { url: "#",
          icon: "fa fa-google",
          btnColor: "btn-info",
          msg: "Sign in with Google"
        },
    ];

    ngOnDestroy(){
      this.subcription.unsubscribe();
    }
}



