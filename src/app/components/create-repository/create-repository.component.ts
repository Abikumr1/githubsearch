import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
    selector: 'gs-create-repository',
    templateUrl: './create-repository.component.html'
  })
export class CreateRepositoryComponent implements OnInit{
   
    createRepository: FormGroup;
    errorMessages: string;

    private validationMessages = {
      required: "This is required field.",
      email: "Please enter a vaild email.",
      minlength: "Please enter minimum 8 characters."
    }
  
    constructor(public activeModal: NgbActiveModal,
                private formBuilder: FormBuilder) {}

    ngOnInit(){
      this.createRepository = this.formBuilder.group({
        name: ['',[Validators.required, Validators.minLength(3)]],
        description: [''],
        private: false
      });

      const nameControl: AbstractControl = this.createRepository.get('name');
      nameControl.valueChanges.pipe(
        debounceTime(500)
      ).subscribe(value => this.setMessage(nameControl));
    }

    create(){
      //console.log(this.createRepository.value)
      this.activeModal.close(this.createRepository.value)
    }

    setMessage(control: AbstractControl){
      this.errorMessages = '';
      if((control.dirty || control.touched) && control.errors){
        this.errorMessages = Object.keys(control.errors).map(
          key => this.validationMessages[key]
        ).join('');
      }
    }
  }