import { CreateRepositoryComponent } from "./create-repository.component"
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

fdescribe('Model Component',()=>{
    let component: CreateRepositoryComponent;
    let fixture: ComponentFixture<CreateRepositoryComponent>

    beforeEach(()=>{
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, NgbModule ],
            declarations: [ CreateRepositoryComponent ],
            //entryComponents: [ CreateRepoComponent ]
        });

        fixture = TestBed.createComponent(CreateRepositoryComponent);
        
        component = fixture.componentInstance;
        component.ngOnInit();
    })

    // it('should create login component', ()=>{
    //     expect(component).toBeTruthy();
    // });

    it('form should invalid when empty',()=>{
        expect(component.createRepository.invalid).toBeTruthy();
    });

    describe('Name field',()=>{
        let name: AbstractControl;

        beforeEach(()=>{
            name = component.createRepository.get('name');
        });

        it('Should invalid when empty',()=>{
            expect(name.valid).toBeFalsy();
        });

        it('Should throw required error when empty',()=>{

            let error = name.errors;

            expect(error['required']).toBeTruthy();
        });

        it('Should not throw required error when field has value',()=>{
            name.setValue("abc");
            let errors = name.errors || {};

            expect(errors['required']).toBeFalsy();
        })

        it('Should contain minimum 8 characters',()=>{
            name.setValue("aqw");
            let errors = name.errors || {};

            expect(errors['minLength']).toBeFalsy();
        })
    });

})