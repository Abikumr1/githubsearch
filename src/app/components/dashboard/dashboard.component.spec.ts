import { DashboardComponent } from "./dashboard.component"
import { User } from '../../module/shared/model';

describe('DashboardComponent',()=>{
    let component: DashboardComponent;
    let PROFILE: User;
    let mockDashboardService;

    beforeEach(()=>{
        PROFILE = loggedUser;
        mockDashboardService = jasmine.createSpyObj(['getLogedInUser','goHome','searchUser','createRepository']);

        //component = new DashboardComponent(mockDashboardService);
    })
})

//#region logged User mock data
const loggedUser = {
    avatar_url: "https://avatars0.githubusercontent.com/u/22312663?v=4",
    bio: null,
    blog: "",
    company: null,
    email: "abhirjit2010@gmail.com",
    events_url: "https://api.github.com/users/Abhi4you/events{/privacy}",
    followers: 0,
    followers_url: "https://api.github.com/users/Abhi4you/followers",
    following: 0,
    following_url: "https://api.github.com/users/Abhi4you/following{/other_user}",
    gists_url: "https://api.github.com/users/Abhi4you/gists{/gist_id}",
    gravatar_id: "",
    hireable: null,
    html_url: "https://github.com/Abhi4you",
    id: 22312663,
    location: "Bangalore",
    login: "Abhi4you",
    name: "Abhishek Kumar",
    node_id: "MDQ6VXNlcjIyMzEyNjYz",
    organizations_url: "https://api.github.com/users/Abhi4you/orgs",
    public_gists: 0,
    public_repos: 8,
    received_events_url: "https://api.github.com/users/Abhi4you/received_events",
    repos_url: "https://api.github.com/users/Abhi4you/repos",
    site_admin: false,
    starred_url: "https://api.github.com/users/Abhi4you/starred{/owner}{/repo}",
    subscriptions_url: "https://api.github.com/users/Abhi4you/subscriptions",
    type: "User",
    url: "https://api.github.com/users/Abhi4you",
    repos: [],
    loggedUser: true
}

//#endregion