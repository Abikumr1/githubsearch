import { Component, OnInit, OnDestroy } from '@angular/core';
import { GitHubService } from 'src/app/service/github.service';
import { User } from '../../module/shared/model'
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { CreateRepositoryComponent } from '../create-repository/create-repository.component';
import { AlertDialogService } from '../../module/shared/alert/alert-dialog.service';
import { ConfirmationDialogService } from '../../module/shared/confirmation-dialog/confirmation-dialog.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'gs-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

  temp: any;
  profile: User;
  loggedUser: string = '';
  privateRepo: boolean = false;
  private subcription: Subscription = new Subscription();

  constructor(private gitService: GitHubService,
    private modalService: NgbModal,
    private config: NgbModalConfig,
    private alertDailog: AlertDialogService,
    private confirmDailog: ConfirmationDialogService) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.getLogedInUser();
    //this.getRepos();
  }

  getLogedInUser() {

    this.subcription.add(this.gitService.getSignInUser().subscribe(
      user => {
        this.loggedUser = user[0].name
        user[0].repos = user[1]
        this.profile = user[0];
        this.profile.loggedUser = true;
      },
      error => {},
      () => {}
    ))
  }

  goHome() {
    this.getLogedInUser();
  }

  searchUser(findUser) {
    this.subcription.add(this.gitService.searchByUser(findUser).subscribe(
      user => {
        user[0].repos = user[1]
        this.profile = user[0];
      }
    ))
  }

  createRepository() {
    const modalRef = this.modalService.open(CreateRepositoryComponent, { size: 'lg' });
    //const createRepo = modalRef.componentInstance.create();

    modalRef.result.then((result) => {
      this.subcription.add(this.gitService.createRepo(result).subscribe(value => {
        this.alertDailog.alert({
          type: 'success',
          message: `${result.name} successfully created`
        })
        this.getLogedInUser();
      },
        error => {
          console.log(error.error.errors[0].message)
          this.alertDailog.alert({
            type: 'error',
            message: error.error.errors[0].message
          })
        }

      ))
    })
    .catch();

  }

  deleteRepository(repoName) {

    this.confirmDailog.confirm('Confirmation', 'Are you sure you want to delete this repository?')
      .then((confirmed) => {

        if (confirmed) {
          this.subcription.add(this.gitService.deleteRepository(repoName).subscribe(
            value => {
              this.alertDailog.alert({
                type: 'success',
                message: `${repoName} successfully deleted.`
              })
            },
            error => {
              this.alertDailog.alert({
                type: 'error',
                message: error
              })
            },
            () => {
              setTimeout(() => {
                this.getLogedInUser();
              }, 500);
            }
          ))
        }
      })
      .catch();
  }

  ngOnDestroy(){
    this.subcription.unsubscribe();
  }

}
