import { Routes } from "@angular/router";
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthGuard } from './module/shared/guards/auth.guard';


export const routes: Routes = [
    { path: 'signin', component: LoginComponent},
    { path:'', redirectTo: '/signin', pathMatch: 'full'},
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]}

]

