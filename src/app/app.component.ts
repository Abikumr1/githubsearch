import { Component, OnInit } from '@angular/core';
import { GitHubService } from './service/github.service';
import { User } from './module/shared/model';

@Component({
  selector: 'gs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  profiles: User[];
  constructor(private gitService: GitHubService){}

  ngOnInit(): void {
    // this.gitService.getUser().subscribe(value => {
    //   //this.profiles = value;
    //   console.log(value);
    // })
  }
}
