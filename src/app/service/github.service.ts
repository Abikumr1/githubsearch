import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { User, AccessToken } from '../module/shared/model';
import { environment } from 'src/environments/environment';
import { forkJoin } from 'rxjs';
import { GitHubUrl } from './github.urls';
import { retry, tap, map } from 'rxjs/operators';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class GitHubService extends BaseService{

  
  getGitAuthToken(authCode){

    let data = {
      client_id: environment.gitAuth.client_id,
      client_secret: environment.gitAuth.client_secret,
      code: authCode
    }

    return this._makeRequest<AccessToken>(GitHubUrl.gitAccessToken, 'POST', data).pipe(
      //tap(value => console.log(value))
    )
  }

  getToken(): string{
    return sessionStorage.getItem('token');
  }

  loggedIn(): boolean{
    return !!sessionStorage.getItem('token');
  }

  getSignInUser(){

    let users = this._makeRequest<User>(GitHubUrl.loggedUser, 'GET');
    let repos = this._makeRequest<any>(GitHubUrl.loggedUserRepos, 'GET');

    return forkJoin([users, repos]);
  }

  
  getRepos(){
    //return this.http.get<any>()
  }

  searchByUser(user){

    let searchUser = this._makeRequest<User>(`${GitHubUrl.searchUser}/${user}`);
    let searchRepo = this._makeRequest<any>(`${GitHubUrl.searchUser}/${user}/repos`);

    return forkJoin([searchUser, searchRepo]);
  }

  createRepo(obj){

    return this._makeRequest<any>(GitHubUrl.loggedUserRepos, obj, 'POST');
  }

  deleteRepository(repoName: string){

    return this._makeRequest(`${GitHubUrl.deleteloggedUserRepo}/Abhi4you/${repoName}`, 'DELETE');

  }
}
