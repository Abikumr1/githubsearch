import { environment } from 'src/environments/environment';

export class GitHubUrl{

    private static readonly baseUrl = environment.Base_URL;
    private static readonly developerUrl = environment.Developer_URL;

    static readonly gitAccessToken = `/login/oauth/access_token`;

    static readonly loggedUser = `${GitHubUrl.developerUrl}/user`;
    static readonly loggedUserRepos = `${GitHubUrl.developerUrl}/user/repos`;

    static readonly searchUser = `${GitHubUrl.developerUrl}/users`;
    static readonly searchUserRepos = `${GitHubUrl.developerUrl}/users`

    static readonly deleteloggedUserRepo = `${GitHubUrl.developerUrl}/repos`
}