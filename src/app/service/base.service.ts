import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map, mergeMap, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

export type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';

export class BaseService {

    constructor(private http: HttpClient) { }

    /**
     * Common request function with basic mapping.
     * @param url Request URL.
     * @param body Body of the request if any
     * @param options An optional extra information object as per the request method.
     * @param method Request method.
     */
    _makeRequest<T>(
        url: string,
        method: HttpMethod = 'GET',
        body?: any,
        options?: {
            skipMap?: boolean;
            responseType?: string;
            headers?: HttpHeaders;
            params?: HttpParams;
            observe?: 'body' | 'events' | 'response';
        }): Observable<T> {

        const http = this.http;
        //const mapper = (res) => <T>res;
        let request;

        switch (method) {
            case 'POST':
            case 'PUT':
            case 'DELETE':
                const requestOptions = {
                    responseType: options && options.responseType as any || 'json',
                    headers: options && options.headers || new HttpHeaders(),
                    params: options && options.params || null,
                    observe: options && options.observe || 'body'
                };
                const args = (method === 'DELETE') ? [url, requestOptions] : [url, body, requestOptions];

                // if(environment.production){
                //     if(!this.csrfService.csrfNonce){
                //         request = this.csrfService.getNonce()
                //         .pipe(
                //             mergeMap(nonce => {
                //                 requestOptions.headers = requestOptions.headers.set(this.csrfService.csrfHeaderName, nonce);
                //                 this.csrfService.csrfNonce = nonce;
                //                 return this._getHttpFn(method).apply(http, args);
                //             })
                //         );
                //     }else{
                //         requestOptions.headers = requestOptions.headers.set(this.csrfService.csrfHeaderName, this.csrfService.csrfNonce);
                //         request = this._getHttpFn(method).apply(http, args);
                //     }
                // }else{
                    request = this._getHttpFn(method).apply(http, args); 
                //}
                break;
            case 'GET':
            default:
                request = http.get(url);


        }

        return request.pipe(
            //tap( value => console.log(value))
        )
        //return options && options.skipMap ? request : request.pipe(map(mapper));
    }

    private _getHttpFn(method: HttpMethod) {
        const http = this.http;
        let fn = this.http.post;

        switch (method) {
            case 'POST':
                fn = http.post;
                break;
            case 'PUT':
                fn = http.put;
                break;
            case 'DELETE':
                fn = http.delete;
        }

        return fn;
    }
}
