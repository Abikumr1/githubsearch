import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { Alert } from '../model';


@Injectable({
    providedIn: 'root'
})
export class AlertDialogService {
    private subject = new Subject<any>();
    constructor() { }

    alert(alertObj: Alert) {
        this.setConfirmation(alertObj);
    }

    setConfirmation(alert: Alert) {
       // let that = this;
        this.subject.next({
            type: alert.type,
            text: alert.message,
            
        });

    }

    setConfirmationNull(){
        this.subject.next();
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}  