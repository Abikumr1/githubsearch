import { Component } from '@angular/core';  
import { AlertDialogService } from "./alert-dialog.service";  
  
@Component({  
    selector: 'alert-dialog',  
    templateUrl: './alert-dialog.comonent.html',  
    styleUrls: ['alert-dialog.component.css']  
})  
export class AlertDialogComponent {  
    message: any;  
    constructor( private alertDialogService: AlertDialogService ) { }  
  
    ngOnInit() {  
        //this function waits for a message from alert service, it gets   
        //triggered when we call this from any other component  
        this.alertDialogService.getMessage().subscribe(message => {  
            this.message = message;  
        });  
    } 
    
    getAlertClass(){
        let type = this.message.type;
        switch(type){
            case "error":
                return { class: 'alert-danger', icon: 'fa fa-times-circle'};
            case "success":
                return { class:'alert-success', icon: 'fa fa-check-circle'};
            default:
                return '';
        }
    }

    close(){
        this.alertDialogService.setConfirmationNull();
    }
}  