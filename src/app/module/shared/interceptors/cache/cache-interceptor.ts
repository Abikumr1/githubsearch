import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable()
export class NoCacheInterceptorService implements HttpInterceptor{

    intercept(request: HttpRequest<any>, next: HttpHandler){

        const httpRequest = request.clone({
            setHeaders : {
              'Access-Control-Allow-Origin' : '*',
              'Cache-Control': 'no-cache',
              'Pragma': 'no-cache',
              'Expires': '0',
              'Accept': 'application/json'
            }
          });

        return next.handle(httpRequest).pipe(
            //tap(value => console.log('header: ',value))
        );
    }
}