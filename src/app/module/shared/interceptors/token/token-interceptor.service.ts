import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { GitHubService } from 'src/app/service/github.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private gitService: GitHubService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler){

    let tokenizedReq = request.clone({
      setHeaders: {
        Authorization: `token ${this.gitService.getToken()}`,      
      }
    });
    //console.log(tokenizedReq)
    return next.handle(tokenizedReq).pipe(
      //tap(value => `Authorization ${value}`)
    );
  }
}
