import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { GitHubService } from '../../../service/github.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private gitService: GitHubService,
    private router: Router) { }

  canActivate(): boolean {

    if (this.gitService.loggedIn())
      return true;
    else {
      this.router.navigate(['/signin'])
        return false;

    }
  }
}
