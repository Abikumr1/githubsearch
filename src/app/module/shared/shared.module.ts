import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AlertDialogComponent } from './alert/alert-dialog.component';
import { BlankPipe } from './pipes/blank.pipe';
import { TokenInterceptorService } from './interceptors/token/token-interceptor.service';
import { NoCacheInterceptorService } from './interceptors/cache/cache-interceptor';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    AlertDialogComponent,
    BlankPipe,
    ConfirmationDialogComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    AlertDialogComponent,
    BlankPipe
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers:[
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: NoCacheInterceptorService, multi: true }
  ]
})
export class SharedModule { }
