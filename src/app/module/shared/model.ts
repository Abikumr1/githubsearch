export interface User {
    avatar_url: string;
    bio: string;
    blog: string;
    company: string;
    created_at?: Date;
    email: string;
    events_url: string;
    followers: number;
    followers_url: string;
    following: number;
    following_url: string;
    gists_url: string;
    gravatar_id: string;
    hireable: boolean;
    html_url: string;
    id: number;
    location: string;
    login: string;
    name: string;
    type: string;
    updated_at?: Date;
    url: string;
    repos: any[];
    loggedUser: boolean;
}

export interface AccessToken{
    access_token: string;
    token_type: string;
    scope: string;
    error?: string;
    error_description?: string;
    error_uri?: string;

}

export interface Alert{
    type: string;
    message: string;
}