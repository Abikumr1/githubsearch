import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'blank'
})
export class BlankPipe implements PipeTransform {

    transform(str: string): string{
        if(str === '' || str === null)
            return '---';

        return str;
    }
}