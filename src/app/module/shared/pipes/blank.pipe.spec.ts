import { BlankPipe } from "./blank.pipe"

describe('BlankPipe', ()=>{

    let blankPipe = new BlankPipe();

    it('should display --- if string is empty ',()=>{
        expect(blankPipe.transform('')).toEqual('---');
    })

    it('should display same string as it has input',()=>{
        expect(blankPipe.transform('Abhishek Kumar')).toEqual('Abhishek Kumar');
    })
})