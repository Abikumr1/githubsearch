// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //Base_URL: 'https://github.com/',
  //Developer_URL: 'https://api.github.com',

  Base_URL: 'http://localhost:4200',
  Developer_URL: 'http://localhost:4200',
  gitAuth: {
    client_id: 'bd03d44d0fd4e0cd19f3',
    client_secret: 'a187c22e02e0253748bd4f9b8227220ed0d11133',

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
